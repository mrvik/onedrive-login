// We need a server in order to receive token from MS Login
// main will create a server and output the URL to authenticate. After it has been received is done.
package main

import (
    "fmt"
    "net/url"
    "os"
    "strings"
)

var (
    // Set this on linker
    clientID string
    scopes=[]string{
        "User.Read",
        "Files.ReadWrite",
        "Files.ReadWrite.All",
    }
)

const (
    redirectURI="http://localhost:%d/trim"
    MS_PERM_BASE="https://graph.microsoft.com/"
    port=9098
)

func main() {
    tokenChannel:=launchServer(port)
    queryValues:=url.Values{
        "client_id": {clientID},
        "scope": {joinScopes()},
        "response_type": {"token"},
        "redirect_uri": {fmt.Sprintf(redirectURI, port)},
    }
    uri:=url.URL{
        Scheme: "https",
        Host: "login.microsoftonline.com",
        Path: "/common/oauth2/v2.0/authorize",
        RawQuery: queryValues.Encode(),
    }
    fmt.Printf("Open your browser and login on %s\n", uri.String())
    token:=<-tokenChannel //After login, http server will get the auth token and send over the channel
    fmt.Printf("Your token is:\n%s\n", token)
    file,err:=os.OpenFile("token", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
    if err!=nil{
        panic("Cannot write token to file: "+err.Error())
    }
    _, err=file.Write([]byte(token))
    if err!=nil{
        panic("Error writing token to file: "+err.Error())
    }
}

func joinScopes() string {
    fscopes:=make([]string, len(scopes))
    for i,v:=range(scopes) {
        fscopes[i]=MS_PERM_BASE+v
    }
    return strings.Join(fscopes, " ")
}
