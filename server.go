package main

import (
    "context"
    "fmt"
    "net/http"
)

const TRIM_URL_HASH=`
<html>
  <head>
    <script>
      location.href=location.href.replace("#", "?").replace("/trim", "/response")
    </script>
  </head>
  <body>
    <h1>Fixing URL</h1>
  </body>
</html>
`

func launchServer(port int) (tokenChannel <-chan string){
    sendChannel:=make(chan string, 1)
    tokenChannel=sendChannel
    go startServer(port, sendChannel)
    return
}

func startServer(port int, sendChannel chan<- string){
    ctx, cancelFn:=context.WithCancel(context.Background())
    sendOnChannel:=func(rw http.ResponseWriter, req *http.Request){
        values:=req.URL.Query()
        if err:=values.Get("error"); err!=""{
            http.Error(rw, err+"\n"+values.Get("error_description"), http.StatusPreconditionFailed)
            return
        }
        token:=values.Get("access_token")
        if token==""{
            http.Error(rw, "No access token on query!", http.StatusBadRequest)
            return
        }
        defer cancelFn()
        fmt.Fprintf(rw, "Token is\n%s\n", token)
        sendChannel<-token
    }
    http.HandleFunc("/response", sendOnChannel)
    http.HandleFunc("/trim", trimHashHandler)
    closed:=make(chan struct{})
    var server=http.Server{
        Addr: fmt.Sprintf(":%d", port),
    }
    go func(){
        defer close(closed)
        err:=server.ListenAndServe()
        if err!=nil && err!=http.ErrServerClosed {
            panic(err)
        }
    }()
    select{
    case <-closed:
    case <-ctx.Done():
        server.Close()
    }
}

func trimHashHandler(rw http.ResponseWriter, req *http.Request) {
    rw.Write([]byte(TRIM_URL_HASH))
}
